var express = require('express');
var router = express.Router();

router.get('/hello', function(req, res, next) {
  res.render('hello', { title: 'Hello World' });
});

router.get('/form', function (req, res, next) {
  res.render('form', { title: 'Simple form with 3 fields' });
});

router.post('/formdata', function (req, res, next) {
  res.render('formdata', {
    Name: req.body.name,
    Surname: req.body.surname,
    Age: req.body.age
  });
});

router.post('/jsondata', express.json({type: '*/*'}), (req, res) => {
  res.render('formdata', {
    Name: req.body.name,
    Surname: req.body.surname,
    Age: req.body.age
  });
});

module.exports = router;
