const http = require('http');
const url = require('url');

function isNullOrEmpty(value) {
    return !(typeof value === "string" && value.length > 0);
}

function isNumeric(num){
    return !isNullOrEmpty(num) && !isNaN(parseFloat(num));
}

function validate(req, res, parameters) {
    console.log(parameters.query);
    if (!isNumeric(parameters.query.a)) {
        res.writeHead(400);
        res.write('<html><head><meta charset="UTF-8"/></head><body>Błędny parametr: a</body></html>');
        res.end();

        return false;
    } else if (!isNumeric(parameters.query.b)) {
        res.writeHead(400);
        res.write('<html><head><meta charset="UTF-8"/></head><body>Błędny parametr: b</body></html>');
        res.end();

        return false;
    }

    return true;
}

function writeResult(res, parameters, result) {
    res.writeHead(200);

    res.write('<html><head><meta charset="UTF-8"/></head><body>');
    res.write('Parametr a:' + parameters.query.a);
    res.write('<br/>');
    res.write('Parametr b:' + parameters.query.b);
    res.write('<br/>');
    res.write('Wynik:' + result.toFixed(2));
    res.write('</body></html>');

    res.end();
}

function addHandler(req, res) {
    const parameters = url.parse(req.url,true);
    if (validate(req, res, parameters)) {
        writeResult(res, parameters, parseFloat(parameters.query.a) + parseFloat(parameters.query.b));
    }
}

function subHandler(req, res) {
    const parameters = url.parse(req.url,true);
    if (validate(req, res, parameters)) {
        writeResult(res, parameters, parseFloat(parameters.query.a) - parseFloat(parameters.query.b));
    }
}

function mulHandler(req, res) {
    const parameters = url.parse(req.url,true);
    if (validate(req, res, parameters)) {
        writeResult(res, parameters, parseFloat(parameters.query.a) * parseFloat(parameters.query.b));
    }
}

function divHandler(req, res) {
    const parameters = url.parse(req.url,true);
    if (validate(req, res, parameters)) {
        writeResult(res, parameters, parseFloat(parameters.query.a) / parseFloat(parameters.query.b));
    }
}

function noResponse(req, res) {
    res.writeHead(404);
    res.write('Błędna operacja');
    res.end();
}

http.createServer((req, res) => {
    const router = {
        '/add': addHandler,
        '/sub': subHandler,
        '/mul': mulHandler,
        '/div': divHandler,
        'default': noResponse
    };

    let reqUrl = new URL(req.url, 'http://127.0.0.1/');
    let redirectedFunc = router[reqUrl.pathname] || router['default'];
    redirectedFunc(req, res);
}).listen(8080, () => {
    console.log('Server is running at http://127.0.0.1:8080/');
});