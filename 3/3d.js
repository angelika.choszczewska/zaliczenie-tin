function porwonaj(a, b) {
    return a - b;
}

function f(liczby) {
  liczby = liczby.sort(porwonaj);

  if (liczby.length < 2) {
    console.error("Za mało liczb w tablicy");

    return [null, null];
  } else {
    return [liczby[1], liczby[liczby.length-2]]
  }
}

console.log(f([11, 20, 10, 40]));