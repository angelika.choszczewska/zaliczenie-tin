//rekurencja
function silniaR(liczba) {
    if(liczba < 2) {
        return 1;
    } else {
    	return liczba * silniaR(liczba - 1);
	}
}
console.log(silniaR(5))

//iteracja
function silniaI(liczba) {
	if (liczba == 0) {
		return 1;
	} else {
		var wynik = 1;
		while(liczba > 0) {
			wynik *= liczba;
			liczba--;
		}
		return wynik;
	}
}

console.log(silniaI(5));