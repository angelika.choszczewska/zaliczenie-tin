function Student(imie, naziwsko, nrIndeksu, oceny) {
    this.imie = imie;
    this.naziwsko = naziwsko;
    this.nrIndeksu = nrIndeksu;
    this.oceny = oceny;

    this.srednia = function() {
        if (this.oceny.length == 0) {
            return 0;
        } else {
            var suma = 0;
            for(var i in this.oceny) {
                suma += this.oceny[i];
            }

            return suma / this.oceny.length;
        }
    }

    this.wypiszDane = function() {
        console.log("Imie: " + this.imie);
        console.log("Nazwisko: " + this.naziwsko);
        console.log("Nr indeksu: " + this.nrIndeksu);
        console.log("Srednia ocen: " + this.srednia());
    }
}

const studentA = new Student("Jan", "Kowalski", "abcd1", [2,3,4,5]);
studentA.wypiszDane(); 

const studentB = new Student("Tomasz", "Wolak", "abcd2", [5,4,3,2]);
studentB.wypiszDane();