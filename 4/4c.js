function waliduj() {
    var liczba = document.forms["formularz"]["liczba"].value;
    var niePuste = document.forms["formularz"]["niePuste"].value;
    var czyOk = true;

    if (niePuste.length == 0) {
        document.getElementById('bladPuste').innerHTML = 'Pole nie może być puste';

        czyOk = false
    } else {
        document.getElementById('bladPuste').innerHTML = '';
    }

    if (isNaN(parseInt(liczba))) {
        document.getElementById('bladNumeryczny').innerHTML = 'Pole musi być liczbą';

        czyOk = false
    } else {
        document.getElementById('bladNumeryczny').innerHTML = '';
    }

    return czyOk;
}